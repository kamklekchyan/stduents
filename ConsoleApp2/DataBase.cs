﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class DataBase
    {
        public List<Teacher> CreateTeachers(int count)
        {
            var list = new List<Teacher>(count);
            for (int i = 1; i < count; i++)
            {
                var teacher = new Teacher { name = "T{i}", surname = "T{i}yan" };
                list.Add(teacher);
            }
            return list;
        }
        public List<Student> CreateStudents(int count)
        {
            var list = new List<Student>(count);
            for (int i = 1; i < count; i++)
            {
                var student = new Student { name = "S{i}", surname = "S{i}yan" };
                list.Add(student);
            }
            return list;
        }
    }
}
