﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class Person
    {
        public string name;
        public string surname;
        public int age;
        public string email;
        public string Fullname => $"{surname} {name}";

        public override string ToString()
        {
            return Fullname;
        }
    }
}
