﻿using System;
using System.Collections.Generic;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            DataBase db = new DataBase();
            List<Teacher> teachers = db.CreateTeachers(5);
            List<Student> students = db.CreateStudents(20);
            var manager = new Manager();
            List<Group> groups = manager.CreateGroups("C#", teachers, students);
​
            Console.WriteLine(groups);​
            Console.ReadLine();
        }
    }
}
