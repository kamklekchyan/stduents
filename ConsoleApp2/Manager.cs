﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class Manager
    {
        public void Shuffle(List<Student> students)
        {
            
        }
        public List<Group> CreateGroups(string groupName, List<Teacher> teachers, List<Student> students)
        {
            var groups = new List<Group>(teachers.Count);
            int teachersCount = teachers.Count;
            int studentsCount = students.Count;​​
            foreach (Teacher teacher in teachers)
            {
                var group = new Group();
                group.name = groupName;
                group.teacher = teacher;
                group.students​ = students;
                groups.Add(group);
            }​
            return groups;
        }
    }
}
